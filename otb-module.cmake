#
# Copyright (C) 2005-2024 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

set(DOCUMENTATION "This module deals with image simulation algorithm. Using
objects transmittance and reflectance and sensor characteristics, it can be possible
to generate realistic hyperspectral synthetic set of data. This module includes
PROSPECT (leaf optical properties) and SAIL (canopy bidirectional reflectance)
models, as well as PROSAIL, which is the combination of the two previous ones.")

# otb_module() defines the module dependencies of ExternalTemplate.
# ExternalTemplate depends on:
#   - OTBCommon (base dependency of all modules)
#   - OTBApplicationEngine (because we build an application in the module, see 'app' folder)
# 
# The tests of module ExternalTemplate drag additional dependencies:
#   - OTBTestKernel (needed for any test driver)
#   - OTBCommandLine (needed to run tests on applications)
#   - OTBSWIG (needed to run tests with Python bindings)
#
# The option ENABLE_SHARED is needed because this module creates a shared
# library. It generates a header with usefull export macros
# (ExternalTemplateExport.h), so that other binaries can link to this library.

otb_module(Miscellaneous
  ENABLE_SHARED
  DEPENDS
    OTBCommon
    OTBApplicationEngine
  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine
    OTBSWIG
  DESCRIPTION
    "Miscellaneous as P0 module"
)
