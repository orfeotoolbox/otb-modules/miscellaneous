#
# Copyright (C) 2005-2024 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This script is a prototype for the future CI, it may evolve rapidly in a near future
#This file set the following variable :
# * otb_use_option
# * otb_wrap_option
# * CONFIGURE_OPTIONS

# set (module_build_option
# "OTB_DIR:PATH=$ENV{OTB_INSTALL_DIR}
# OTB_BUILD_MODULE_AS_STANDALONE:BOOL=ON
# BUILD_EXAMPLES:BOOL=ON
# BUILD_SHARED_LIBS:BOOL=ON
# BUILD_TESTING:BOOL=ON")

set (module_build_option
"OTB_DIR:PATH=$ENV{OTB_INSTALL_DIR}
OTB_BUILD_MODULE_AS_STANDALONE:BOOL=ON
BUILD_SHARED_LIBS:BOOL=ON")

if ( NOT ENV{CI_SKIP_TESTING})
  string(APPEND module_build_option "\n BUILD_TESTING:BOOL=ON")
endif()

set (otb_qa_option
"CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON")

set (cmake_configure_option
"CMAKE_BUILD_TYPE=${CTEST_BUILD_CONFIGURATION}
CMAKE_INSTALL_PREFIX:PATH=${CTEST_INSTALL_DIRECTORY}
CMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=TRUE
CMAKE_INSTALL_RPATH:PATH=${CTEST_INSTALL_DIRECTORY}lib")

# include specific OS vars
if((CTEST_SITE) AND EXISTS "${CMAKE_CURRENT_LIST_DIR}/${CTEST_SITE}.cmake")
  # will set its output in 'site_option'
  include("${CMAKE_CURRENT_LIST_DIR}/${CTEST_SITE}.cmake")
endif()

set(concat_options
"${module_build_option}
${cmake_configure_option}
${site_option}
")

if (QA)
  set(concat_options
"${concat_options}
${otb_qa_option}
")
endif()

#Transform the previous string in list
string (REPLACE "\n" ";" otb_options ${concat_options})

foreach(item ${otb_options})
  set( CONFIGURE_OPTIONS "${CONFIGURE_OPTIONS}-D${item};")
endforeach(item)
